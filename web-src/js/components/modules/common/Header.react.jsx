var ModuleConstant = require('../../../constants/ModuleConstant');
var ModuleActions = require('../../../actions/ModuleActions');

var React = require('react');
var DropdownButton = require('react-bootstrap/lib/DropdownButton');
var MenuItem = require('react-bootstrap/lib/MenuItem');
var classnames = require('classnames');

module.exports = React.createClass({
    handleMenuSelected: function (eventKey, href, target) {
        ModuleActions.gotoModule(href);
    },
    render : function(){
        var logoImgStyle = {
            height: '18px'
        };
        var modules = ModuleConstant.modules;
        return (
            <nav id="header" className="navbar navbar-inverse">
                <div id='menu-btn' className={classnames('btn', 'btn-lg', {hidden: this.props.currentModule.id === 'home'})} onMouseEnter={this.props.showLeftNav} onMouseLeave={this.props.hideLeftNav}>
                    <span className="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                </div>
                <div className="container">
                    <div className={classnames('navbar-header', {hidden: this.props.currentModule.id === 'home'})}>
                        <a className="navbar-brand"><span><img alt="Brand" src="/images/home/logo.png" style={logoImgStyle}></img>
                            &nbsp;&nbsp;{this.props.currentModule.label}</span>&nbsp;-&nbsp;Peer To Peer</a>
                    </div>
                    <div className="nav navbar-nav navbar-right">
                        <DropdownButton bsStyle='link' title={userEmail}>
                            <MenuItem href = {modules.profile.id} onSelect = {this.handleMenuSelected}>我的账户</MenuItem>
                            <MenuItem href='/logout'>注销</MenuItem>
                        </DropdownButton>
                    </div>
                </div>
            </nav>
        );
    }
});