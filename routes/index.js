var express = require('express');
var os = require('os');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	if(!req.session.userEmail) {
		return res.redirect('/login');
    }
    res.render('index', { title: 'The PeerJS Test', userEmail: req.session.userEmail});
});

module.exports = router;
