/**
 * Created by mcbird on 15/5/13.
 * 门户路由入口
 */

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	res.render('portal', { userEmail: req.session.userEmail || '请登陆' });
});

module.exports = router;
